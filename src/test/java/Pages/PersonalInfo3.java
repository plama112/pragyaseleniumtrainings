package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PersonalInfo3 {
	WebDriver driver = null;
	WebElement element = null;

	By enterFirstName = By.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_FirstName']");
	By enterMiddleName = By.xpath("//input[@id ='NameAndAddressEdit_embedded_questions_list_MiddleInitial']");
	By enterLastName = By.xpath("//input[@id ='NameAndAddressEdit_embedded_questions_list_LastName']");
	By selectSuffix = By.xpath("//select[@id = 'NameAndAddressEdit_embedded_questions_list_Suffix']");
	By enterDOB = By.xpath("//input[@id = 'NameAndAddressEdit_embedded_questions_list_DateOfBirth']");
	By enterStreetNumberAndName = By
			.xpath("//input[@name='NameAndAddressEdit_embedded_questions_list_MailingAddress']");
	By enterApartment = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']");
	By enterCity = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_City']");
	By enterZipCode = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_ZipCode']");
	By clickpostbox = By.xpath("//input[contains(@type,'checkbox')]");
	By clickQuote = By.xpath("//button[contains(text(),'Okay, start my quote.')]");

	public PersonalInfo3(WebDriver driver) {
		this.driver = driver;

	}

	public void enterFirstName(String FirstName) throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(enterFirstName).sendKeys(FirstName);
	}

	public void enterMiddleName(String MiddleName) {
		driver.findElement(enterMiddleName).sendKeys(MiddleName);
	}

	public void enterLastName(String LastName) {
		driver.findElement(enterLastName).sendKeys(LastName);
	}

	public void selectSuffix(String Suffix) {
		element = driver.findElement(selectSuffix);
		Select chooseSuffix = new Select(element);
		chooseSuffix.selectByVisibleText(Suffix);

	}

	public void enterDOB(String DOB) {
		driver.findElement(enterDOB).sendKeys(DOB);
		driver.findElement(enterDOB).sendKeys(Keys.TAB);

	}

	public void enterStreetNumberAndName(String StreetNumber) throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(enterStreetNumberAndName).sendKeys(StreetNumber);
	}

	public void enterApartment(String Apartment) {
		driver.findElement(enterApartment).sendKeys(Apartment);
	}

	public void enterCity(String City) {
		driver.findElement(enterCity).sendKeys(City);
		driver.findElement(enterCity).sendKeys(Keys.TAB);
	}

	public void enterZipCode(String Zipcode) {
		driver.findElement(enterZipCode).sendKeys(Zipcode);
		driver.findElement(enterZipCode).sendKeys(Keys.TAB);
	}

	public void clickpostbox() {
		Thread.onSpinWait();
		driver.findElement(clickpostbox).click();
		driver.findElement(clickpostbox).sendKeys(Keys.TAB);
	}

	public void clickQuote() {
		driver.findElement(clickQuote).click();
	}
}
