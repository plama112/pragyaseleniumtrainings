package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Drivers5 {
	WebElement element = null;
	WebDriver driver = null;
	By clickFemaleGender = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_F']");
	By clickMaleGender = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']");
	// By clickGender = By
	// .xpath("//label[@id='DriversAddPniDetails_embedded_questions_list_Gender_Label']");
	By selectMaritalStatus = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']");
	By selectEducationBackground = By
			.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']");
	By selectEmploymentStatus = By
			.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']");
	By enterSocialSecurity = By
			.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_SocialSecurityNumber']");
	By SelectPrimaryResidence = By
			.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']");
	By selectMovedin = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_HasPriorAddress']");
	By selectUSLicensedStatus = By.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_LicenseStatus']");
	By selectHistoryofDriving = By
			.xpath("//select[@id='DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']");
	By clickClaims = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']");
	By clickTickets = By.xpath("//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']");
	By clickContinue = By.xpath("//button[text()='Continue']");

	public Drivers5(WebDriver driver) {
		this.driver = driver;

	}

	public void clickFemaleGender(String Value) throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(clickFemaleGender).click();

	}

	public void selectMaritalStatus(String Marital) {
		element = driver.findElement(selectMaritalStatus);
		Select chooseMarital = new Select(element);
		chooseMarital.selectByVisibleText(Marital);

	}

	public void selectEducationBackground(String Education) throws InterruptedException {
		Thread.sleep(1100);
		element = driver.findElement(selectEducationBackground);
		Select chooseEducation = new Select(element);
		chooseEducation.selectByIndex(4);
	}

	public void selectEmploymentStatus(String Employment) {
		element = driver.findElement(selectEmploymentStatus);
		Select chooseEmployment = new Select(element);
		chooseEmployment.selectByIndex(5);
	}

	public void enterSocialSecurity(String SocialSecurity) throws InterruptedException {
		Thread.sleep(1000);
		driver.findElement(enterSocialSecurity).sendKeys(SocialSecurity);
	}

	public void selectPrimaryResidence(String Residence) {
		element = driver.findElement(SelectPrimaryResidence);
		Select chooseResidence = new Select(element);
		chooseResidence.selectByIndex(5);

	}

	public void selectMovedin(String Movein) {
		element = driver.findElement(selectMovedin);
		Select chooseMovein = new Select(element);
		chooseMovein.selectByVisibleText(Movein);
	}

	public void selectUSLicensedStatus(String Licensed) throws InterruptedException {
		Thread.sleep(1100);
		element = driver.findElement(selectUSLicensedStatus);
		Select chooseLicensedStatus = new Select(element);
		chooseLicensedStatus.selectByVisibleText(Licensed);
	}

	public void selectHistoryofDriving(String Driving) {
		element = driver.findElement(selectHistoryofDriving);
		Select chooseHistory = new Select(element);
		chooseHistory.selectByIndex(1);

	}

	public void clickClaims(String Value) {
		driver.findElement(clickClaims).click();
		;
	}

	public void clickTickets(String Value) {
		driver.findElement(clickTickets).click();
	}

	public void clickContinue() throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(clickContinue).sendKeys(Keys.ENTER);
	}
}
