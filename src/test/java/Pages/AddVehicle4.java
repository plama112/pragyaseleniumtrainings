package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AddVehicle4 {
	WebElement element = null;
	WebDriver driver = null;

	By clickVehicleYear = By.xpath("//*[@id=\"VehiclesNew_embedded_questions_list_Year\"]/ul/li[5]");
	By clickVehicleMake = By.xpath("//*[@id=\"VehiclesNew_embedded_questions_list_Make\"]/ul/li[12]");
	By clickVehicleModel = By.xpath("//*[@id=\"VehiclesNew_embedded_questions_list_Model\"]/ul/li[1]");
	By selectBodyType = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']");
	By selectPrimaryUse = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']");
	By selectOwnOrLease = By.xpath("//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']");
	By selectVehicleOwnership = By.xpath("//select[@id= 'VehiclesNew_embedded_questions_list_LengthOfOwnership']");
	By clickDone = By.xpath("//button[text()='Done']");
	By clickContinue = By.xpath("//button[text()='Continue']");

	public AddVehicle4(WebDriver driver) {
		this.driver = driver;

	}

	public void clickVehicleYear(String Year) throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(clickVehicleYear).click();

	}

	public void clickVehicleMake(String Make) {
		driver.findElement(clickVehicleMake).click();
	}

	public void clickVehicleModel(String Model) {
		driver.findElement(clickVehicleModel).click();
	}

	public void selectBodyType(String Type) {
		element = driver.findElement(selectBodyType);
		Select chooseBodyType = new Select(element);
		chooseBodyType.selectByVisibleText(Type);
	
	}

	public void selectPrimaryUse(String Primary) throws InterruptedException {
		Thread.sleep(1100);
		element = driver.findElement(selectPrimaryUse);
		Select chooseUse = new Select(element);
		chooseUse.selectByIndex(1);

	}

	public void selectOwnorLease(String Own) throws InterruptedException {
		Thread.sleep(1100);
		element = driver.findElement(selectOwnOrLease);
		Select chooseOwn = new Select(element);
		chooseOwn.selectByIndex(2);
	}

	public void selectVehicleOwnership(String Ownership) throws InterruptedException {
		element = driver.findElement(selectVehicleOwnership);
		Select chooseOwnership = new Select(element);
		chooseOwnership.selectByIndex(3);
	}

	public void clickDone() throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(clickDone).click();
	}

	public void clickContinue() throws InterruptedException {
		Thread.sleep(1100);
		driver.findElement(clickContinue).sendKeys(Keys.ENTER);
	}
}
