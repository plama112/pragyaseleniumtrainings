package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Google {
	static WebDriver driver = null;

	WebElement element = null;
	
     By txtBoxSearch = By.xpath("//input[contains(@name,'q')]");
	

	 By searchBtn = By.xpath("//input[contains(@name,'btnK')]");
	
	
	
	
	public Google(WebDriver driver) {

		this.driver = driver;

	}
	
	public void googleSearch(String searchText) throws InterruptedException {

		Thread.sleep(5000);

		driver.findElement(txtBoxSearch).sendKeys(searchText);
		driver.findElement(searchBtn).sendKeys(Keys.RETURN);
		
	}
	
    public static boolean isElementPresent(By by) {
		
		boolean check = false;
		
		try {
			driver.findElement(by);
			check = true;
			return true; 
		}

		catch (NoSuchElementException e) {
			check = false;;
			return false;

		}
	}

}
