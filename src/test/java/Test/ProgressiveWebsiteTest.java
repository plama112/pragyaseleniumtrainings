package Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.AddVehicle4;
import Pages.Drivers5;
import Pages.HomePage1;
import Pages.PersonalInfo3;
import Pages.Zipcode2;

public class ProgressiveWebsiteTest {
	public static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
		// Step1: InvokeBrowser
		invokeBrowser();
		// Step2: Perform actions on
		SignUpFlow();

	}

	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();
		driver.navigate().to("https://www.progressive.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Progessive")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");
			System.exit(0);
		}
	}

	public static void SignUpFlow() throws InterruptedException {
		HomePage1 obj = new HomePage1(driver);
		obj.clickAuto();

		Zipcode2 obj2 = new Zipcode2(driver);
		obj2.enterZipcode("75062");
		obj2.clickAuto();

		PersonalInfo3 obj3 = new PersonalInfo3(driver);
		obj3.enterFirstName("Anjali");
		obj3.enterMiddleName("");
		obj3.enterLastName("Lama");
		obj3.selectSuffix("Jr");
		obj3.enterDOB("04/11/1995");
		obj3.enterStreetNumberAndName("2729 Vancouver St");
		obj3.enterApartment("");
		obj3.enterCity("");
		obj3.enterZipCode("");
		obj3.clickpostbox();
		obj3.clickQuote();

		AddVehicle4 obj4 = new AddVehicle4(driver);
		obj4.clickVehicleYear("2018");
		obj4.clickVehicleMake("Genesis");
		obj4.clickVehicleModel("G80");
		obj4.selectBodyType("4DR 6CYL");
		obj4.selectPrimaryUse("1");
		obj4.selectOwnorLease("2");
		obj4.selectVehicleOwnership("3");
		obj4.clickDone();
		obj4.clickContinue();

		Drivers5 obj5 = new Drivers5(driver);
		obj5.clickFemaleGender("");
		obj5.selectMaritalStatus("Single");
		obj5.selectEducationBackground("4");
		obj5.selectEmploymentStatus("5");
		obj5.enterSocialSecurity("123-45-1234");
		obj5.selectPrimaryResidence("5");
		obj5.selectMovedin("No");
		obj5.selectUSLicensedStatus("Valid");
		obj5.selectHistoryofDriving("1");
		obj5.clickClaims("No");
		obj5.clickTickets("No");
		obj5.clickContinue();

	}

	public static void closeBrowser() {
		System.exit(0);
	}
}
