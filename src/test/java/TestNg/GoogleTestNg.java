package TestNg;


import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class GoogleTestNg {
	static WebDriver driver;

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();

	}

	public static void navigateTOGoogle() {

		driver.navigate().to("https://www.google.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	@Test
	public static void performAction() {

		// navigate to google.com
		navigateTOGoogle();

		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		WebElement SearchBtn = driver.findElement(By.xpath("//input[@name='btnK']"));
		txtBoxSearch.sendKeys("What is TestNG");
		SearchBtn.sendKeys(Keys.RETURN);

	}

	@AfterTest

	public static void terminateTest() {
		driver.close();
		driver.quit();
		System.out.println("Terminated the Browser");

	}

}
