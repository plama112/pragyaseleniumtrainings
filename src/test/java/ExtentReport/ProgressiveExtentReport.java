package ExtentReport;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import Pages.AddVehicle4;
import Pages.Drivers5;
import Pages.HomePage1;
import Pages.PersonalInfo3;
import Pages.Zipcode2;

public class ProgressiveExtentReport {

	
	// Global Variables
		ExtentHtmlReporter htmlReporter;
		static ExtentReports extent;
		ExtentTest test;
		//WebDriver driver;

		static WebDriver driver;

		@BeforeSuite
		
		public void setUp() {
		// creating ExtentReporter object and creating new extentReport Html file
		// creating ExtentReports and attach the reporter(s)
		htmlReporter = new ExtentHtmlReporter("ProgressiveWebsite.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		}

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();

	}

	public static void navigateTOProgressive() {

		driver.navigate().to("https://www.progressive.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Progressive")) {
			System.out.println(driver.getTitle() + "-------------------was launched");
		} else {
			System.out.println("Fail Browser was not invoked----------------------");
			System.exit(0);
		}
		ExtentTest isBrowserOpened = extent.createTest("Invoke Browser",
				"This websites ensures that the browser is invoked");
		isBrowserOpened.pass("Browser was invoked as Expected");

	}

	@Test(priority = 0)
	public static void PragyaLama() {
		navigateTOProgressive();

		ExtentTest test= extent.createTest("Progressive Homepage", "This Page ensures it is launched");
		test.log(Status.INFO, "Homepage Launched");

		// creating a page object
		ExtentTest test1 = extent.createTest("Pragya Lama","This page is launched");
		test1.log(Status.INFO, "Homepage launched");

		HomePage1 obj = new HomePage1(driver);
		obj.clickAuto();
		test1.pass("clickAuto");
		
	}

	@Test(priority = 1)
	public static void Zipcode() throws InterruptedException {
		ExtentTest test2 = extent.createTest("Progressive ZipCode", "This Page ensures the zipcode page is opened");
		test2.log(Status.INFO, "Zipcode Page");

		Zipcode2 obj2 = new Zipcode2(driver);
		obj2.enterZipcode("75062");
		test2.pass("enterZipcode");
		obj2.clickAuto();
		test2.pass("clickAuto");
		Thread.sleep(1100);
	}

	@Test(priority = 2)
	public static void PersonalInfo() throws InterruptedException {
		ExtentTest test3 = extent.createTest("Progressive PersonalInfo","This page allows us to fillup personal info");
	    test3.log(Status.INFO, "Personal Info Page Launched");
		PersonalInfo3 obj3 = new PersonalInfo3(driver);
		obj3.enterFirstName("Anjali");
		test3.pass("enterFirstName");
		obj3.enterMiddleName("");
		test3.pass("enterMiddleName");
		obj3.enterLastName("Lama");
		test3.pass("enterLastName");
		obj3.selectSuffix("Jr");
		test3.pass("selectSuffix");
		obj3.enterDOB("04/11/1995");
		test3.pass("enterBOB");
		Thread.sleep(1100);
		obj3.enterStreetNumberAndName("2729 Vancouver St");
		test3.pass("enterStreetNumberAndName");
		obj3.enterApartment("");
		test3.pass("enterApartment");
		obj3.enterCity("");
		test3.pass("enterCity");
		obj3.enterZipCode("");
		test3.pass("enterZipCode");
		obj3.clickpostbox();
		Thread.sleep(1100);
		test3.pass("clickpostbox");
		obj3.clickQuote();
		test3.pass("clickQuote");
	}
	
	@Test(priority = 3)
	public static void ProgressiveAddVehicle() throws InterruptedException {
		ExtentTest test4 = extent.createTest("Progressive Vehicle","This page allows us to add vehicle");
	    test4.log(Status.INFO, "Progressive Vehicle Page Launched");
		
		AddVehicle4 obj4 = new AddVehicle4(driver);
		obj4.clickVehicleYear("2018");
		test4.pass("clickVehicleYear");
		obj4.clickVehicleMake("Genesis");
		test4.pass("clickVehicleMake");
		obj4.clickVehicleModel("G80");
		test4.pass("clickVehicleModel");
		obj4.selectBodyType("4DR 6CYL");
		test4.pass("selectBodyType");
		Thread.sleep(1100);
		obj4.selectPrimaryUse("1");
		test4.pass("selectPrimaryUse");
		obj4.selectOwnorLease("2");
		test4.pass("selectOwnorLease");
		obj4.selectVehicleOwnership("3");
		test4.pass("selectVehicleOwnership");
		obj4.clickDone();
		test4.pass("clickDone");
		Thread.sleep(1100);
		obj4.clickContinue();
		test4.pass("clickContinue");
	}

	@Test(priority = 4)
	public static void ProgressiveDrivers() throws InterruptedException {
		ExtentTest test5 = extent.createTest("Progressive Drivers","This page allows us to add more drivers");
	    test5.log(Status.INFO, "Progressive Drivers Page Launched");
		Thread.sleep(1100);
		Drivers5 obj5 = new Drivers5(driver);
		obj5.clickFemaleGender("");
		test5.pass("clickFemaleGender");
		obj5.selectMaritalStatus("Single");
		test5.pass("selectMaritalStatus");
		obj5.selectEducationBackground("4");
		test5.pass("selectEducationBackground");
		obj5.selectEmploymentStatus("5");
		test5.pass("selectEmploymentStatus");
		Thread.sleep(1100);
		obj5.enterSocialSecurity("123-45-1234");
		test5.pass("enterSocialSecurity");
		obj5.selectPrimaryResidence("5");
		test5.pass("selectPrimaryResidence");
		obj5.selectMovedin("No");
		test5.pass("selectMovedin");
		obj5.selectUSLicensedStatus("Valid");
		test5.pass("selectUSLicensedStatus");
		Thread.sleep(1100);
		obj5.selectHistoryofDriving("1");
		test5.pass("selectHistoryofDriving");
		obj5.clickClaims("No");
		test5.pass("clickClaims");
		obj5.clickTickets("No");
		test5.pass("clickTickets");
		obj5.clickContinue();
		test5.pass("clickContinue");
	}

	@AfterTest

	public static void terminateBrowser() throws InterruptedException {
		ExtentTest test6 = extent.createTest("Progressive Homepage","This page is closed");
        test6.log(Status.INFO, "Terminate the Browser");
        Thread.sleep(1000);
		driver.close();
		driver.quit();


	}

	@AfterSuite
	public void tearDown() {
		extent.flush();

	}

}




